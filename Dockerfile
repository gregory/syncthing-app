FROM cloudron/base:0.10.0
MAINTAINER Syncthing Developers <support@cloudron.io>

EXPOSE 8000

RUN apt-get update && apt-get -y install busybox

ENV NGINX_VERSION=1.12.0
ENV NGINX_LDAP_VERSION=b80942160417e95adbadb16adc41aaa19a6a00d9

# Build a custom nginx with ldap support
RUN apt-get remove -y nginx-full && apt-get autoremove -y && apt-get -y install libldap2-dev libpcre3-dev
RUN mkdir -p /tmp/nginx-ldap
WORKDIR /tmp/nginx-ldap
RUN wget "https://github.com/kvspb/nginx-auth-ldap/archive/${NGINX_LDAP_VERSION}.tar.gz" -O - \
    | tar -xz -C /tmp/nginx-ldap --strip-components=1

RUN mkdir -p /tmp/nginx
WORKDIR /tmp/nginx
RUN wget "https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz" -O - \
    | tar -xz -C /tmp/nginx --strip-components=1
RUN ./configure \
    --add-dynamic-module=/tmp/nginx-ldap \
    --modules-path=/usr/local/nginx/modules \
    --conf-path=/app/code/nginx.conf \
    --pid-path=/run/nginx.pid \
    --error-log-path=/run/nginx.error.log \
    --build=cloudron-river
RUN make install

ENV VERSION 0.14.29

RUN mkdir -p /app/code \
    && wget https://github.com/syncthing/syncthing/releases/download/v${VERSION}/syncthing-linux-amd64-v${VERSION}.tar.gz -O - \
     | tar -xz -C /app/code --strip-components=1

WORKDIR /app/code

ADD nginx.conf /app/code/nginx.conf
ADD inittab /etc/inittab
ADD start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]
