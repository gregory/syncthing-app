#!/usr/bin/env node

'use strict';

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    webdriver = require('selenium-webdriver');

var by = webdriver.By,
    until = webdriver.until;

var accessKey = 'admin',
    secretKey = 'secretkey';

var bucket_prefix = 'bucket',
    bucket_id = 0,
    bucket;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    var chrome = require('selenium-webdriver/chrome');
    var server, browser = new chrome.Driver();
    var username = process.env.USERNAME, password = process.env.PASSWORD;

    before(function (done) {
        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    var LOCATION = 'test';
    var TEST_TIMEOUT = 30000;
    var SYNC_PORT = 22001;
    var app;

    function pageLoaded() {
        return browser.wait(until.titleMatches(/[0-9a-f]{12} \| Syncthing/), TEST_TIMEOUT);
    }

    function visible(selector) {
        return browser.wait(until.elementLocated(selector), TEST_TIMEOUT).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(selector)), TEST_TIMEOUT);
        });
    }

    function loadPage(callback) {
        browser.manage().deleteAllCookies();
        browser.get('https://' + username + ':' + password + '@' + app.fqdn);
        return pageLoaded().then(function() {
            callback();
        });
    }

    function addFolder(callback) {
        return pageLoaded().then(function() {
            browser.findElement(by.css('[ng-click*=addFolder]')).click();
            return visible(by.id('folderPath')).then(function() {
            	return browser.findElement(by.id('folderLabel')).sendKeys('test');
            }).then(function() {
	        return browser.findElement(by.id('folderPath')).sendKeys('/app/data/test');
            }).then(function() {
                //Clear and re-enter the folder to avoid race with auto-completion
                return browser.findElement(by.id('folderPath')).clear();
            }).then(function() {
	        return browser.findElement(by.id('folderPath')).sendKeys('/app/data/test');
            }).then(function() {
                return browser.findElement(by.css('[ng-click*=saveFolder]')).click();
            }).then(function() {
                return browser.wait(until.elementLocated(by.css('#folders .panel-status span[ng-switch-when=unshared]')), TEST_TIMEOUT);
            }).then(function() {
                callback();
            });
        });
    }

    function removeFolder(callback) {
        browser.get('https://' + username + ':' + password + '@' + app.fqdn);
        return pageLoaded().then(function() {
            browser.findElement(by.css('#folders button')).click();
            setTimeout(function() {
                browser.findElement(by.css('#folder-0 button[ng-click*=editFolder]')).click();
                setTimeout(function() {
                    browser.findElement(by.css('[ng-click*=deleteFolder]')).click().then(function() {
                        setTimeout(callback, 1000); //This needs to run for some time
                    });
                }, 1000); //No way to check for visibility of angular-js components
            }, 1000); //No way to check for visibility of angular-js components
        });
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        execSync('cloudron install --new --wait --port-bindings PORT=' + SYNC_PORT + ' --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can load page', loadPage);
    it('can add folder', addFolder);
    it('can remove folder', removeFolder);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can load page', loadPage);
    it('can add folder', addFolder);
    it('can remove folder', removeFolder);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --wait --location ' + LOCATION + '2', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can load page', loadPage);
    it('can add folder', addFolder);
    it('can remove folder', removeFolder);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

});
