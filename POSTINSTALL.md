This application integrates with Cloudron authentication.
However, all Cloudron users share the same Syncthing session.
Please note that only the username is accepted as login and not the email address.

Syncthing contains an internal user `admin` that is needed by Cloudron but not exposed. 
Please do not change the password of that account.

Please create all sync folders as subfolders of `/app/data` as this is the only writable folder.

