#!/bin/bash

set -eu

mkdir -p /app/data/config

export STNODEFAULTFOLDER=1 STNOUPGRADE=1

# if this if the first run, generate a useful config
if [ ! -f /app/data/config/config.xml ]; then
  echo "=> Generating config"
  /app/code/syncthing --generate="/app/data/config"
fi

cat >/run/ldap.conf <<EOF
ldap_server cloudron {
  url ${LDAP_URL}/${LDAP_USERS_BASE_DN}?username;
  binddn ${LDAP_BIND_DN};
  binddn_passwd ${LDAP_BIND_PASSWORD};
  group_attribute ${LDAP_GROUPS_BASE_DN};
  group_attribute_is_dn on;
  require valid_user;
}
EOF

# Set the listenAddress and the gui enabled to make sure user doesnt lock themselves  out by accident.
sed -e 's,<listenAddress>.*</listenAddress>,<listenAddress>tcp://:22000</listenAddress>,' -i /app/data/config/config.xml
sed -e 's,<gui .*>,<gui enabled="true" tls="false" debugging="false">,' -i /app/data/config/config.xml

chown -R cloudron:cloudron /app/data/config /app/data

exec busybox init

